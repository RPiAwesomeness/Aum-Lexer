# Aum-Lexer

Lexer for the [Aum](https://gitlab.com/RPiAwesomeness/Aum) language.

Some code adapted from https://github.com/chr4/writing_an_interpreter_in_rust