use tokens;
use tokens::TokenType;

use std::iter::Peekable;
use std::str::Chars;

// TODO: Use these to detect newlines to support windows as well as *nix
#[cfg(windows)]
const LINE_BREAK: &'static str = "\r\n";
#[cfg(not(windows))]
const LINE_BREAK: &'static str = "\n";

/// Handles the tokenization of the source file
pub struct Lexer {
    input: Peekable<Chars<'static>>,
    absolute: u32,
    char_pos: u32,
    line: u32,
}

/// Keeps track of a token with the character position and line it exists it
#[derive(Debug)]
pub struct Token {
    pub token: TokenType,
    pub absolute: u32,
    pub char_pos: u32,
    pub line: u32,
}

impl Lexer {
    /// Creates a new Lexer with the input string converted into a Peekable of chars
    pub fn new(input: &'static str) -> Lexer {
        Lexer {
            input: input.chars().peekable(),
            absolute: 0,
            char_pos: 0,
            line: 1,
        }
    }

    /// Attempts to read the next character in the input
    pub fn read_char(&mut self) -> Option<char> {
        // Incremement the character and absolute position
        self.char_pos += 1;
        self.absolute += 1;

        // Read and return the next character
        self.input.next()
    }

    /// Peeks at the next character in the input and returns a reference to that char without consuming it
    pub fn peek_char(&mut self) -> Option<&char> {
        self.input.peek()
    }

    /// Peeks at the next character in the input and compares it against the character provided
    fn peek_char_is(&mut self, ch: char) -> bool {
        match self.peek_char() {
            Some(&c) => c == ch,
            None => false,
        }
    }

    /// Consumes characters until a non-whitespace character is detected
    fn skip_whitespace(&mut self) {
        while let Some(&c) = self.peek_char() {
            // If the peeked character is non-whitespace...
            if !c.is_whitespace() {
                // ...stop skipping
                break;
            }

            self.read_char();

            // If a new line is hit then increment the line count and reset the character position
            if c == '\n' {
                self.line += 1;
                self.char_pos = 0;
            }
        }
    }

    /// Non-consumingly checks if the next character is a letter
    fn peek_is_letter(&mut self) -> bool {
        match self.peek_char() {
            Some(&ch) => ch.is_alphabetic(),
            None => false,
        }
    }

    /// Non-consumingly checks if the next character is a valid next character in an identifier
    fn peek_is_valid_identifier(&mut self) -> bool {
        self.peek_is_letter() || self.peek_char_is('_')
    }

    /// Reads an identifier into a string, consuming characters
    fn read_identifier(&mut self, first: char) -> String {
        // Create the String we'll be returning
        let mut ident = String::new();
        ident.push(first);

        // Keep going while the characters are alphanumeric
        while self.peek_is_valid_identifier() {
            ident.push(self.read_char().unwrap());
        }

        // Return the identifier
        ident
    }

    /// Reads characters until non-numeric characters are reached
    fn read_number(&mut self, first: char) -> Option<i32> {
        let mut number = String::new();
        number.push(first);

        while let Some(&c) = self.peek_char() {
            if !c.is_numeric() {
                break;
            }

            number.push(self.read_char().unwrap());
        }

        match number.parse::<i32>() {
            Ok(val) => Some(val),
            Err(err) => {
                eprintln!("{}", err);
                None
            }
        }
    }

    pub fn next_token(&mut self) -> Token {
        // Skip leading whitespace
        self.skip_whitespace();

        let mut token = Token {
            token: TokenType::Illegal,
            absolute: self.absolute,
            char_pos: self.char_pos,
            line: self.line,
        };

        match self.read_char() {
            // Operators
            Some('=') => {
                if self.peek_char_is('=') {
                    // Consume the next equal sign, it's an equality operator instead of assignment
                    self.read_char();
                    token.token = TokenType::Equal;
                    token
                } else if self.peek_char_is('>') {
                    self.read_char();
                    token.token = TokenType::DoubleArrowRight;
                    token
                } else {
                    token.token = TokenType::Assign;
                    token
                }
            }
            Some('+') => {
                token.token = TokenType::Plus;
                token
            }
            Some('-') => {
                if self.peek_char_is('>') {
                    self.read_char();
                    token.token = TokenType::ArrowRight;
                    token
                } else {
                    token.token = TokenType::Minus;
                    token
                }
            }
            Some('*') => {
                token.token = TokenType::Star;
                token
            }
            Some('/') => {
                token.token = TokenType::Slash;
                token
            }
            Some('\\') => {
                token.token = TokenType::BackSlash;
                token
            }
            Some('!') => {
                if self.peek_char_is('=') {
                    self.read_char();
                    token.token = TokenType::NotEqual;
                    token
                } else {
                    token.token = TokenType::Bang;
                    token
                }
            }
            Some('#') => {
                token.token = TokenType::Hash;
                token
            }
            Some('>') => {
                if self.peek_char_is('=') {
                    self.read_char();
                    token.token = TokenType::GreaterThanEqTo;
                    token
                } else {
                    token.token = TokenType::GreaterThan;
                    token
                }
            }
            Some('<') => {
                if self.peek_char_is('=') {
                    self.read_char();
                    token.token = TokenType::LessThanEqTo;
                    token
                } else if self.peek_char_is('-') {
                    self.read_char();
                    token.token = TokenType::ArrowLeft;
                    token
                } else {
                    token.token = TokenType::LessThan;
                    token
                }
            }

            // Logical Operators
            Some('&') => {
                if self.peek_char_is('&') {
                    self.read_char();
                    token.token = TokenType::And;
                    token
                } else {
                    token.token = TokenType::Ampersand;
                    token
                }
            }
            Some('|') => {
                if self.peek_char_is('|') {
                    self.read_char();
                    token.token = TokenType::Or;
                    token
                } else {
                    token.token = TokenType::Pipe;
                    token
                }
            }

            // Delimeters
            Some('.') => {
                token.token = TokenType::Period;
                token
            }
            Some(',') => {
                token.token = TokenType::Comma;
                token
            }
            Some('(') => {
                token.token = TokenType::OpenParen;
                token
            }
            Some(')') => {
                token.token = TokenType::CloseParen;
                token
            }
            Some('{') => {
                token.token = TokenType::OpenBrace;
                token
            }
            Some('}') => {
                token.token = TokenType::CloseBrace;
                token
            }
            Some('[') => {
                token.token = TokenType::OpenBracket;
                token
            }
            Some(']') => {
                token.token = TokenType::CloseBracket;
                token
            }
            Some(':') => {
                if self.peek_char_is('=') {
                    self.read_char();
                    token.token = TokenType::Initialize;
                    token
                } else {
                    token.token = TokenType::Colon;
                    token
                }
            }
            Some(';') => {
                token.token = TokenType::Semicolon;
                token
            }
            Some('"') => {
                let mut val = String::new();

                // Consume characters until the close quotation mark is found
                while !self.peek_char_is('"') {
                    if let Some(ch) = self.read_char() {
                        if ch == '\\' {
                            if self.peek_char_is('"') {
                                // It's an escaped quote, read it in and move on
                                self.read_char();

                                // Add just the quoted
                                val.push('"');

                                // Move on to the next character
                                continue;
                            }
                            // TODO: Handle other escape sequences here
                        }

                        val.push(ch);
                    } else {
                        // Reached end of file without closing the string, illegal token and return early
                        token.token = TokenType::Illegal;
                        return token;
                    }
                }

                // Reached the end of the quoted string
                // Consume the quote character
                self.read_char();

                token.token = TokenType::Str(val);
                token
            }
            // Other
            Some(ch @ _) => {
                if is_valid_identifier_char(ch) {
                    // If it's an f and the next character is a " then it's the start of a format string
                    // if (ch == 'f' && self.peek_char_is('"')) {
                    //     self.read_char();
                    //     return Token::FormatStringPrefix;
                    // }

                    // Not a format string prefix, read the rest into an identifier
                    let literal = self.read_identifier(ch);
                    token.token = tokens::lookup_ident(&literal);
                    token
                } else if ch.is_numeric() {
                    // if ch == '0' {
                    //     if self.peek_char_is('x') {
                    //         self.read_char();
                    //         return Token::HexPrefix;
                    //     } else if self.peek_char_is('b') {
                    //         self.read_char();
                    //         return Token::BinaryPrefix;
                    //     }
                    // }
                    token.token = match self.read_number(ch) {
                        Some(int) => TokenType::Integer(int),
                        None => TokenType::Illegal
                    };

                    token
                } else {
                    let mut foo = String::new();
                    foo.push(ch);
                    for _ in 0..4 {
                        foo.push(self.read_char().unwrap());
                    }
                    println!("Illegal: {}", foo);
                    token.token = TokenType::Illegal;
                    token
                }
            }

            // Handle EOF
            None => {
                token.token = TokenType::EndOfFile;
                token
            }
        }
    }
}

/// Validates whether the passed character is a valid identifier
pub fn is_valid_identifier_char(ch: char) -> bool {
    ch.is_alphabetic() || ch == '_'
}
