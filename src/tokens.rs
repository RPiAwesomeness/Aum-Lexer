#[derive(Debug, Clone, PartialEq)]
pub enum TokenType {
    // Control tokens
    Illegal,
    EndOfFile,

    // Literals
    Ident(String), // Any identifier (eg. myVar)
    // Numeric(String), // Any numeric type (decimal: 123, hexadecimal: 0x10c, binary: 0b1010)
    // Currently only ints are supported and are another token
    Integer(i32),
    Str(String), // String type

    // Literal identifiers/prefixes
    HexPrefix,          // 0x...
    BinaryPrefix,       // 0b...
    FormatStringPrefix, // f"..."

    // Operators
    Assign,           // =
    Plus,             // +
    Minus,            // -
    Star,             // *
    Slash,            // /
    BackSlash,        // \
    Bang,             // !
    Hash,             // #
    GreaterThan,      // >
    LessThan,         // <
    GreaterThanEqTo,  // >=
    LessThanEqTo,     // <=
    Equal,            // =
    NotEqual,         // !=
    ArrowRight,       // ->
    ArrowLeft,        // <-
    DoubleArrowRight, // =>
    Initialize,       // :=

    // Logical Operators
    And, // &&
    Or,  // ||

    // Delimeters
    Period,       // .
    Comma,        // ,
    OpenParen,    // (
    CloseParen,   // )
    OpenBracket,  // [
    CloseBracket, // ]
    OpenBrace,    // {
    CloseBrace,   // }
    Ampersand,    // &
    Pipe,         // |
    Semicolon,    // ;
    Colon,        // :

    // Keywords
    Function, // fn
    If,       // if
    Else,     // else
    True,     // true
    False,    // false
    Mutable,  // mut
    Return,   // return
}

impl Default for TokenType {
    // Default identifier is illegal, thus any TokenType that is created needs to override this
    fn default() -> TokenType {
        TokenType::Illegal
    }
}

/// Takes an identifier string and attempts to match it against defined keywords.
///
/// # Returns
/// Keyword TokenType that matches the passed the identifier, otherwise if it matches no keywords then an Ident TokenType is
/// returned storing the value that was passed in
pub fn lookup_ident(ident: &str) -> TokenType {
    match ident {
        "fn" => TokenType::Function,
        "if" => TokenType::If,
        "else" => TokenType::Else,
        "true" => TokenType::True,
        "false" => TokenType::False,
        "mut" => TokenType::Mutable,
        "return" => TokenType::Return,
        _ => TokenType::Ident(ident.to_string()),
    }
}
