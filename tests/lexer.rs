extern crate aum_lexer;

use aum_lexer::lexer;
use aum_lexer::tokens::TokenType;

#[test]
fn next_token_test() {
    #[cfg_attr(rustfmt, rustfmt_skip)]
    let token_input = "five := 5;

fn(x, y) {
    x + y;
}

_ten -> 7 => bar <- fiber;
!= == < > <= >= * / \\ - ! # && ||

if (5 < \"test\") {
    return true;
} else {
    return false;
}

bar[0:]
";

    let token_test = vec![
        TokenType::Ident("five".into()),
        TokenType::Initialize,
        TokenType::Integer(5),
        TokenType::Semicolon,
        TokenType::Function,
        TokenType::OpenParen,
        TokenType::Ident("x".into()),
        TokenType::Comma,
        TokenType::Ident("y".into()),
        TokenType::CloseParen,
        TokenType::OpenBrace,
        TokenType::Ident("x".into()),
        TokenType::Plus,
        TokenType::Ident("y".into()),
        TokenType::Semicolon,
        TokenType::CloseBrace,
        TokenType::Ident("_ten".into()),
        TokenType::ArrowRight,
        TokenType::Integer(7),
        TokenType::DoubleArrowRight,
        TokenType::Ident("bar".into()),
        TokenType::ArrowLeft,
        TokenType::Ident("fiber".into()),
        TokenType::Semicolon,
        TokenType::NotEqual,
        TokenType::Equal,
        TokenType::LessThan,
        TokenType::GreaterThan,
        TokenType::LessThanEqTo,
        TokenType::GreaterThanEqTo,
        TokenType::Star,
        TokenType::Slash,
        TokenType::BackSlash,
        TokenType::Minus,
        TokenType::Bang,
        TokenType::Hash,
        TokenType::And,
        TokenType::Or,
        TokenType::If,
        TokenType::OpenParen,
        TokenType::Integer(5),
        TokenType::LessThan,
        TokenType::Str("test".into()),
        TokenType::CloseParen,
        TokenType::OpenBrace,
        TokenType::Return,
        TokenType::True,
        TokenType::Semicolon,
        TokenType::CloseBrace,
        TokenType::Else,
        TokenType::OpenBrace,
        TokenType::Return,
        TokenType::False,
        TokenType::Semicolon,
        TokenType::CloseBrace,
        TokenType::Ident("bar".into()),
        TokenType::OpenBracket,
        TokenType::Integer(0),
        TokenType::Colon,
        TokenType::CloseBracket,
    ];

    let mut lex = lexer::Lexer::new(token_input);
    for tok in token_test {
        let read = lex.next_token();
        assert_eq!(read.token, tok);
    }
}

#[test]
fn token_position_test() {
    // Test string
    #[cfg_attr(rustfmt, rustfmt_skip)]
    let pos_input = "five := 5;
foo -> 7

==
";

    // Known positions the values should be
    // (abs, char, line)
    let pos_test = vec![
        (0, 0, 1),  // five
        (5, 5, 1),  // :=
        (8, 8, 1),  // 5
        (9, 9, 1),  // ;
        (11, 0, 2), // foo
        (15, 4, 2), // ->
        (18, 7, 2), // 7
        (21, 0, 4), // ==
    ];

    let mut lex = lexer::Lexer::new(pos_input);

    for pos in pos_test {
        let read = lex.next_token();
        println!("{:?}", read);
        assert_eq!(read.absolute, pos.0);
        assert_eq!(read.char_pos, pos.1);
        assert_eq!(read.line, pos.2);
    }
}
