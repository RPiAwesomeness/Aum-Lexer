extern crate aum_lexer;

use aum_lexer::tokens::{lookup_ident, TokenType};

#[test]
fn ident_lookup_test() {
    struct IdentLookup<'a> {
        ident: &'a str,
        token: TokenType,
    }

    let tests: Vec<IdentLookup> = vec![
        IdentLookup {
            ident: "fn",
            token: TokenType::Function,
        },
        IdentLookup {
            ident: "if",
            token: TokenType::If,
        },
        IdentLookup {
            ident: "else",
            token: TokenType::Else,
        },
        IdentLookup {
            ident: "true",
            token: TokenType::True,
        },
        IdentLookup {
            ident: "false",
            token: TokenType::False,
        },
        IdentLookup {
            ident: "mut",
            token: TokenType::Mutable,
        },
        IdentLookup {
            ident: "return",
            token: TokenType::Return,
        },
    ];

    for ident in &tests {
        assert_eq!(lookup_ident(ident.ident), ident.token);
    }
}
